(function () {
  'use strict';

  angular
    .module('app', [
      'app.core',
      'app.login',
      'app.orders',
      'app.report',
      'app.history',
      'app.deliveries',
    ]);
})();
