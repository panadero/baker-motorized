(function () {
  'use strict';

  angular
    .module('app.deliveries')
    .config(deliveriesRoutes);

  /* @ngInject */
  function deliveriesRoutes($stateProvider) {
    $stateProvider
      .state('deliveries', {
        url: '/deliveries',
        templateUrl: 'app/deliveries/deliveries.html',
        controller: 'DeliveriesController',
        controllerAs: 'vm',
        cache: false,
      });
  }
})();
