(function () {
  'use strict';

  angular
    .module('app.deliveries')
    .controller('DeliveriesController', DeliveriesController);

  function DeliveriesController(ordersService,
                                $state,
                                $ionicLoading,
                                $ionicPopup) {
    var _this = this;
    _this.return = backToOrders;
    _this.searchOrders = searchOrders;
    _this.dateFormat = dateFormat;
    _this.deliveries = [];

    function searchOrders() {
      if (_this.from && _this.to) {
        $ionicLoading.show();
        ordersService.getDeliveriesOrders(_this.from.getTime(), _this.to.getTime())
          .then(function (response) {
            $ionicLoading.hide();
            _this.deliveries = response.data;
          })
          .catch(noInternet);
      }
    }

    function dateFormat(date) {
      return date.substring(0, 10);
    }

    function backToOrders() {
      $state.go('orders');
    }

    function noInternet(response) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Verifique su conexión a Internet',
        template: 'No hemos podido cargar las ordenes, verifique su conexión e intente nuevamente',
        cssClass: 'text-center',
      });
    }
  }
})();
