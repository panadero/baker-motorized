(function () {
  'use strict';

  angular
    .module('app.report')
    .config(reportRoutes);

  function reportRoutes($stateProvider) {
    $stateProvider
      .state('report', {
        url: '/report',
        templateUrl: 'app/reports/reports.html',
        controller: 'ReportsContoller',
        controllerAs: 'vm',
        cache: false,
      });
  }
})();
