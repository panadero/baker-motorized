(function () {
  'use strict';

  angular
    .module('app.report')
    .controller('ReportsContoller', ReportsContoller);

  function ReportsContoller(ordersService,
                            $ionicLoading,
                            $ionicPopup,
                            $state) {
    var _this = this;
    _this.orders = ordersService.getOrdersTomorrow();
    _this.breadOrders = {};
    _this.return = backToOrders;
    _this.fullBreadOrders = {};
    _this.hasSomeData = hasSomeData;
    var retry = 0;
    tryGetBreadOrders();

    function tomorrowBreadOrders(response) {
      $ionicLoading.hide();
      ordersService.setBreadOrders(response.data);
      var allBreadOrders = ordersService.getBreadOrders();
      var breadsTomorrow = {};

      for (var order in _this.orders) {
        for (var breadOrder in allBreadOrders) {
          if (allBreadOrders[breadOrder].order.id == _this.orders[order].id) {
            _this.breadOrders[order] = allBreadOrders[breadOrder];
            _this.breadOrders[order].bread.breadType = ordersService.getSpanishNames(_this.breadOrders[order].bread.breadType);
          }
        }
      }

      _this.fullBreadOrders = _this.breadOrders;
      sortByBread();
    }

    function sortByBread() {
      var breadsTomorrow = {};
      for (var order in _this.breadOrders) {
        var breadName = _this.breadOrders[order].bread.breadType;
        if (breadsTomorrow[breadName]) {
          breadsTomorrow[breadName][0] += _this.breadOrders[order].amount;
        } else {
          breadsTomorrow[breadName] = [_this.breadOrders[order].amount, _this.breadOrders[order].bread.breadType];
        }
      }

      _this.breadOrders = breadsTomorrow;
    }

    function backToOrders() {
      $state.go('orders');
    }

    function tryGetBreadOrders() {
      if (retry++ < 10) {
        $ionicLoading.show();
        ordersService.getAllBreadOrders()
          .then(tomorrowBreadOrders)
          .catch(tryGetBreadOrders);
      } else {
        $ionicLoading.hide();
        noInternet();
      }
    }

    function noInternet() {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Verifique su conexión a Internet',
        template: 'No hemos podido cargar las ordenes, verifique su conexión e intente nuevamente',
        cssClass: 'text-center',
      });
    }

    function hasSomeData() {
      return Object.keys(_this.fullBreadOrders).length > 0;
    }
  }

})();
