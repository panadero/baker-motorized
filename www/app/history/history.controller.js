(function () {
  'use strict';

  angular
    .module('app.history')
    .controller('HistoryController', HistoryController);

  function HistoryController(ordersService,
                             $ionicLoading,
                             $ionicPopup,
                             $state) {
    var _this = this;
    _this.inRangeLength =  0;
    _this.inRangeOrders = [];
    _this.orderByRange = orderByRange;
    _this.dateFormat = dateFormat;
    _this.return = backToOrders;
    _this.inRangeBreads = {};
    _this.totalPrice = totalPrice;
    _this.amount = {};
    _this.searchBreadOrders = searchBreadOrders;
    _this.allBreadOrders = [];
    _this.allOrders = [];
    _this.filteredOrders = [];
    _this.breadCounters = {};

    function searchBreadOrders() {
      if (_this.from && _this.to) {
        _this.filteredOrders = [];
        _this.breadCounters = {
          regular: 0,
          regularDelivered: 0,
          special: 0,
          specialDelivered: 0,
          eliminated: 0
        };
        $ionicLoading.show();
        var dayInMilliseconds = 86399000;
        ordersService.getBreadOrdersByDates(_this.from.getTime(), (_this.to.getTime() + dayInMilliseconds))
          .then(function (response) {
            $ionicLoading.hide();
            ordersService.setBreadOrders(response.data);
            _this.allBreadOrders = response.data;
            _this.allOrders = _this.allBreadOrders.map(function(breadOrder) {
              var order = breadOrder.order;
              if (order && order.orderType.id !== 3) {
                _this.filteredOrders.push(order)
              }
              return breadOrder.order;
            });
            orderByRange();
            buildOrderDetailCounters();
          })
          .catch(noInternet);
      }
    }

    function orderByRange() {
      _this.inRangeBreads  = {};
      _this.amount = {};
      _this.inRangeOrders = _this.allBreadOrders.map(function(breadOrder) {
        return breadOrder.order;
      });
      _this.inRangeLength = _this.inRangeOrders.length;

      angular.forEach(_this.allBreadOrders, function(breadOrder, key) {
        if (_this.inRangeBreads[breadOrder.bread.breadType] && breadOrder.order.orderType.id != 3 && breadOrder.order.state) {
          _this.inRangeBreads[breadOrder.bread.breadType][0] += breadOrder.amount;
        } else if (breadOrder.order.orderType.id != 3 && breadOrder.order.state) {
          _this.inRangeBreads[breadOrder.bread.breadType] = [breadOrder.amount, ordersService.getSpanishNames(breadOrder.bread.breadType)];
        }
      });
      totalPrice();
    }

    function dateFormat(date) {
      return date.substring(0, 10);
    }

    function backToOrders() {
      $state.go('orders');
    }

    function buildOrderDetailCounters() {
      angular.forEach(_this.inRangeOrders, function(order, key) {
        switch(order.orderType.id) {
          case 1:
            _this.breadCounters.regular++;
            if (order.state) {
              _this.breadCounters.regularDelivered++;
            }
            break;
          case 2:
            _this.breadCounters.special++;
            if (order.state) {
              _this.breadCounters.specialDelivered++;
            }
            break;
          case 3:
            _this.breadCounters.eliminated++;
            break;
        }
      });
    }

    function totalPrice() {
      var total = 0;
      for (var bread in _this.inRangeBreads) {
        var keyValue = getObjectKey(_this.inRangeBreads, bread);
        var partial = _this.inRangeBreads[bread][0] * _this.amount[keyValue];
        total += partial || 0;
      }

      return total || 0;
    }

    function getObjectKey(object, key) {
      var count = 0;
      for (var attribute in object) {
        if (object[attribute][1] == ordersService.getSpanishNames(key)) {
          return count;
        }

        count++;
      }
    }

    function noInternet(response) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Verifique su conexión a Internet',
        template: 'No hemos podido cargar las ordenes, verifique su conexión e intente nuevamente',
        cssClass: 'text-center',
      });
    }
  }
})();
