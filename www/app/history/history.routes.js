(function () {
  'use strict';

  angular
    .module('app.history')
    .config(historyRoutes);

  function historyRoutes($stateProvider) {
    $stateProvider
      .state('history', {
        url: '/history',
        templateUrl: 'app/history/history.html',
        controller: 'HistoryController',
        controllerAs: 'vm',
        cache: false,
      });
  }
})();
