(function () {
  'use strict';

  angular
    .module('app.orders')
    .controller('OrdersCtrl', OrdersCtrl);

  /* @ngInject */
  function OrdersCtrl(ordersService,
                      $ionicPopup,
                      $ionicLoading,
                      $state,
                      CORE) {

    var _this = this;
    _this.toggleGroup = toggleGroup;
    _this.isGroupShown = isGroupShown;
    _this.orders = [];
    _this.sortedOrders = {};
    _this.sendPush = sendPush;
    _this.noOrders = false;
    _this.ordersTomorrow = {};
    _this.validateAdmin = validateAdmin;
    _this.logout = logout;
    _this.isAdmin = isAdmin;
    _this.goToDeliveries = goToDeliveries;
    _this.buildOrderDetailsForOrder = buildOrderDetailsForOrder;
    _this.allBreadOrders = [];
    var retry = 0;
    var allOrders;
    var skipIds = [];
    var currentGroup;
    var usedTokens = '';
    var recurrentCardToken;
    activate();

    function activate() {
      $ionicLoading.show();
      ordersService.getAllBreadOrders()
        .then(breadOrdersCallback)
        .catch(tryGetBreadOrders)
    }

    function tryGetBreadOrders() {
      if (retry++ < 10) {
        ordersService.getAllBreadOrders()
          .then(breadOrdersCallback)
          .catch(tryGetBreadOrders);
      } else {
        $ionicLoading.hide();
        noInternet();
      }
    }

    function breadOrdersCallback(response) {
      $ionicLoading.hide();
      _this.allBreadOrders = response.data;
      var orders = response.data.map(function(breadOrder) {
        return breadOrder.order;
      });
      todayOrders(orders);
    }

    function todayOrders(orders) {
      var date = new Date().toLocaleDateString("en-US");
      var deliveriesToday = JSON.parse(localStorage.getItem('todayOrders'));
      if (deliveriesToday) {
        var deliveries = deliveriesToday[date];
        if (deliveries && deliveries.length > 0) {
          skipIds = deliveries;
        }
      }

      $ionicLoading.hide();
      allOrders = orders;
      var orderCount = 0;
      var tomorrowCount = 0;
      for (var order in orders) {
        if (isToday(orders[order].deliveryDate, orders[order].orderType.id, orders[order].state, true, orders[order].id)) {
          _this.orders[orderCount++] = orders[order];
        } else if (isTomorrow(orders[order])) {
          _this.ordersTomorrow[tomorrowCount++] = orders[order];
        }
      }

      if (_this.orders.length > 0) {
        orderByHouse();
      } else {
        _this.noOrders = true;
      }
    }

    function isToday(date, orderType, orderState, findToday, orderId) {
      var addTomorrow = 0;
      if (orderType == 3) {
        return false;
      }

      if (!findToday) {
        addTomorrow++;
      }

      var skipIdIncluded = skipIds.indexOf(orderId);

      if (date && (skipIdIncluded === -1)) {
        var today = new Date();
        today.setHours(0,0,0,0);
        var orderDate = new Date(date.substring(0, 4), date.substring(5, 7) - 1, date.substring(8, 10));
        if (orderState != true && today.getMonth() == orderDate.getMonth() && (today.getDate() + addTomorrow) == orderDate.getDate()) {
          return true;
        } else if ((orderType == 1) && (orderState != true) && (orderDate.getDay() == (today.getDay() + addTomorrow)) && (today.getTime() >= orderDate.getTime())) {
          return true;
        }
      }

      return false;
    }

    function isTomorrow(order) {
      var date = order.deliveryDate;
      var today = new Date();
      var orderDate = new Date(date.substring(0, 4), date.substring(5, 7) - 1, date.substring(8, 10));
      var tomorrowDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
      if (order.orderType.id !== 3) {
        if (order.orderType.id === 1) {
          return orderDate.getDay() == tomorrowDate.getDay();
        } else if (order.orderType.id === 2) {
          return orderDate.getFullYear() == tomorrowDate.getFullYear() &&
                  orderDate.getMonth() == tomorrowDate.getMonth() &&
                  orderDate.getDate() == tomorrowDate.getDate();
        }
      } else {
        return false;
      }
    }

    function orderByHouse() {
      var houses = {};
      var tokens = {};
      for (var number in _this.orders) {
        if (!houses[_this.orders[number].client.houseNumber]) {
          houses[_this.orders[number].client.houseNumber] = _this.orders[number].id + ' ';
          tokens[_this.orders[number].client.houseNumber] = _this.orders[number].pushToken;
        } else {
          houses[_this.orders[number].client.houseNumber] += _this.orders[number].id + ' ';
        }
      }

      for (var house in houses) {
        var items = []
        houses[house].split(' ').map(function(item) {
          if(item !== "") {
            items.push(item);
          }
        });
        _this.sortedOrders[house] = {
          items: items,
          id: house,
          show: false,
          date: new Date().toString().substring(0, 11),
          sent: false,
          pushId: tokens[house],
        };
      }
    }

    function validateAdmin(isReport) {
      var admin = ordersService.getUser();
      if (isAdmin()) {
        if (isReport) {
          ordersService.saveOrdersTomorrow(_this.ordersTomorrow);
          $state.go('report');
        } else {
          ordersService.saveOrders(allOrders);
          $state.go('history');
        }
      } else {
        $ionicPopup.alert({
          title: 'Ventana de Administrador',
          template: 'Esta ventana es para uso exclusivo de un administrador',
          cssClass: 'text-center', });
      }
    }

    function toggleGroup(group) {
      group.show = !group.show;
    }

    function isGroupShown(group) {
      if (group.sent) {
        return false;
      }

      return group.show;
    }

    function sendPush(group) {
      group.sent = !group.sent;
      if (!group.pushId) {
        changeOrderState(group);
      } else {
        ordersService.notifyUser(group.pushId)
          .then(function (response) {
              changeOrderState(group);
            })
          .catch(function () {
            notUpdated(group);
          });
      }
    }

    function changeOrderState(group) {
      var houseNumber = group.id;
      _this.sortedOrders[group.id];
      currentGroup = group;
      for (var order = 0; order < _this.orders.length; order++) {
        for (var groupOrder in _this.sortedOrders[group.id].items) {
          var currentOrder = _this.sortedOrders[group.id].items[groupOrder];
          if (currentOrder == _this.orders[order].id) {
            ordersService.setTodayDeliveries(_this.orders[order].id);
            _this.orders[order].state = true;
            ordersService.updateOrder(_this.orders[order])
              .then(function(response) {
                if (response.data.orderType.id == 2) {
                  applyCharge(response)
                } else {
                  getUserCharges(response.data);
                }
              })
              .catch(function () {
                notUpdated(group);
              });
          }
        }
      }
    }

    function notUpdated(group) {
      if (!group) {
        group = currentGroup;
      }

      group.sent = false;
    }

    function noInternet() {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Verifique su conexión a Internet',
        template: 'No hemos podido cargar las ordenes, verifique su conexión e intente nuevamente',
        cssClass: 'text-center',
      });
    }

    function applyCharge(order) {
      var today = new Date();
      var date = order.data.deliveryDate;
      var orderDate = new Date(date.substring(0, 4), date.substring(5, 7) - 1, date.substring(8, 10));
      var tokenExists = usedTokens.includes(order.data.chargeToken);
      if (today.getDate() != orderDate.getDate() || today.getMonth() != orderDate.getMonth()) {
        recurrentCardToken = order.data.cardToken;
        getUserCharges(order.data);
        usedTokens += order.data.chargeToken;
      } else if(!tokenExists) {
        usedTokens += order.data.chargeToken;
        ordersService.applyCharge(prepareUserCharge(order.data))
          .then(orderSuccess)
          .catch(function(response) {
            notUpdated(false);
          });
      }
    }

    function getUserCharges(order) {
      ordersService.getUserCharges(order.client, order.chargeToken)
        .then(function (response) {
          includeRecurrentCharge(response, order.client.email);
        });
    }

    function includeRecurrentCharge(response, userName) {
      ordersService.includeCharge(userName, response.data.transactionAmount)
        .then(applyRecurrentCharge);
    }

    function applyRecurrentCharge(order) {
      var recurrentJSON = {
        applicationName: CORE.APPLICATION_NAME,
        applicationPassword: CORE.APPLICATION_PASSWORD,
        userName: order.data.userName,
        chargeTokenId: order.data.chargeTokenId,
        cardTokenId: recurrentCardToken,
      };
      ordersService.applyCharge(recurrentJSON)
        .then(orderSuccess)
        .catch(function () {
          notUpdated(false);
        });
    }

    function prepareUserCharge(order) {
      var json = {
        applicationName: CORE.APPLICATION_NAME,
        applicationPassword: CORE.APPLICATION_PASSWORD,
        userName: order.client.email,
        chargeTokenId: order.chargeToken,
        cardTokenId: order.cardToken,
      };
      return json;
    }

    function orderSuccess(response) {
      if (!response.data.isApproved) {
        notUpdated(false);
      } else {
        $ionicPopup.alert({
          title: 'Orden Entregada!',
          template: 'Orden entregada satisfactoriamente!',
          cssClass: 'text-center',
        });
      }
    }

    function logout() {
      localStorage.removeItem('user')
      $state.go('login', {location:'replace'})
    }

    function goToDeliveries() {
      if (isAdmin()) {
        $state.go('deliveries');
      } else {
        $ionicPopup.alert({
          title: 'Ventana de Administrador',
          template: 'Esta ventana es para uso exclusivo de un administrador',
          cssClass: 'text-center', });
      }
    }

    function isAdmin() {
      var currentUser = ordersService.getUser();
      return (currentUser.user == CORE.ADMIN_USER  && currentUser.password == CORE.ADMIN_PASSWORD);
    }

    function buildOrderDetailsForOrder(orderId) {
      var breadOrder = null;

      for(var i = 0; i < _this.allBreadOrders.length; i++) {
        if (_this.allBreadOrders[i].order.id == orderId) {
          breadOrder = _this.allBreadOrders[i];
          break;
        }
      }

      return breadOrder.amount + " " +ordersService.getSpanishNames(breadOrder.bread.breadType);
    }

  }
})();
