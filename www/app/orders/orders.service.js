(function () {
  'use strict';

  angular
    .module('app.orders')
    .service('ordersService', ordersService);

  /* @ngInject */
  function ordersService(CORE,
                         $http) {
    var service  = {
      url: CORE.API_URL,
      getOrders: getOrders,
      updateOrder: updateOrder,
      notifyUser: notifyUser,
      saveOrders: saveOrders,
      orders: {},
      getAllOrders: getAllOrders,
      setBreadOrders: setBreadOrders,
      getAllBreadOrders: getAllBreadOrders,
      getBreadOrdersByDates: getBreadOrdersByDates,
      getBreadOrders: getBreadOrders,
      getDeliveriesOrders: getDeliveriesOrders,
      breadOrders: {},
      saveOrdersTomorrow: saveOrdersTomorrow,
      getOrdersTomorrow: getOrdersTomorrow,
      ordersTomorrow: {},
      getSpanishNames: getSpanishNames,
      setUser: setUser,
      getUser: getUser,
      user: {},
      applyCharge: applyCharge,
      includeCharge: includeCharge,
      getUserCharges: getUserCharges,
      setTodayDeliveries: setTodayDeliveries,
      todayDeliveries: {},
      orderString: '',
    };

    return service;

    function setUser(user) {
      var userJSON = {
        user: user.user,
        password: user.password,
      };
      localStorage.setItem('user', angular.toJson(userJSON));
    }

    function getUser() {
      return JSON.parse(localStorage.getItem('user'));
    }

    function getOrders() {
      var json = {
        method: 'GET',
        url: service.url + 'order',
      };

      return $http(json);
    }

    function updateOrder(order) {
      var orderJSON = {
        method: 'POST',
        url: service.url + 'order',
        data: order,
      };

      return $http(orderJSON);
    }

    function getAllBreadOrders() {
      var auth = window.btoa("allowAdmin:true");
      var headers = { "Authorization": "Basic " + auth };
      var json = {
        method: 'GET',
        url: service.url + 'breadorder',
        headers: headers
      };
      return $http(json);
    }

    function getBreadOrdersByDates(startDate, endDate) {
      var auth = window.btoa("allowAdmin:true");
      var headers = { "Authorization": "Basic " + auth };
      var json = {
        method: 'GET',
        url: service.url + 'breadorder/'+ startDate + "/" + endDate,
        headers: headers
      };
      return $http(json);
    }

    function getDeliveriesOrders(startDate, endDate) {
      var auth = window.btoa("allowAdmin:true");
      var headers = { "Authorization": "Basic " + auth };
      var json = {
        method: 'GET',
        url: service.url + 'order/deliveries/'+ startDate + "/" + endDate,
        headers: headers
      };
      return $http(json);
    }

    function setBreadOrders(orders) {
      service.breadOrders = orders;;
    }

    function getBreadOrders() {
      return service.breadOrders;
    }

    function notifyUser(token) {
      var json = {
        method: 'POST',
        url: 'https://fcm.googleapis.com/fcm/send',
        headers: {
          'Content-type': 'application/json',
          'Authorization': 'key=AAAAq7XZmkg:APA91bFYWcUJagLtU-k9Harh269KL7S4USa7y-ObFaIe5LW9azwkktMtNQ2GB2GIZtjGRx4uzjUupDrRG9PiBdeYTc5opvV81LpTQbu6IBMzCWkyrJFHZwBOER43mr8EtNinawQvmedd',
        },
        data: {
          to: token,
          notification: {
            body: "Su orden está afuera. Disfrute!",
            title: "Panadero"
          }
        },
      };

      return $http(json);
    }

    function saveOrders(order) {
      service.orders = order;
    }

    function getAllOrders() {
      return service.orders;
    }

    function saveOrdersTomorrow(orders) {
      service.ordersTomorrow = orders;
    }

    function getOrdersTomorrow() {
      return service.ordersTomorrow;
    }

    function getSpanishNames(enNames) {
      var spanishName;
      for (var name = 0; name < enNames.length; name++) {
        switch (enNames) {
          case 'ciabbata':
            spanishName = 'Ciabbata';
            break;
          case 'italian':
            spanishName = 'Italiano';
            break;
          case 'wheat':
            spanishName = 'Integral';
            break;
          case 'cheese':
            spanishName = 'B. Queso';
            break;
          case 'spanish':
            spanishName = 'Español';
            break;
          case 'french':
            spanishName = 'Frances';
            break;
          case 'sesame':
            spanishName = 'B. Ajonjolí';
            break;
          case 'baguette':
            spanishName = 'Baguette';
            break;
          case 'big_roll':
            spanishName = 'Mano Grande';
            break;
        }
      }

      return spanishName;
    }

    function getUserCharges(user, token) {
      var JSON = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/AppVerifyUserCharge',
        data: { applicationName: CORE.APPLICATION_NAME,
                applicationPassword: CORE.APPLICATION_PASSWORD,
                userName: user.email,
                chargeTokenId: token},
      };

      return $http(JSON);
    }
    function includeCharge(username, amount) {
      var transactionPost = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/AppIncludeCharge',
        data: { applicationName: CORE.APPLICATION_NAME,
                applicationPassword: CORE.APPLICATION_PASSWORD,
                chargeDescription: CORE.CHARGE_DESCRIPTION,
                userName: username,
                transactionCurrency: CORE.CURRENCY,
                transactionAmount: amount, },
      };
      return $http(transactionPost);
    }

    function applyCharge(order) {
      var chargeJSON = {
        method: 'POST',
        url: CORE.PAYMENT_SERVICE_URL + '/api/AppApplyCharge',
        headers: {
          'Content-Type': 'application/json',
        },
        data: order,
      };

      return $http(chargeJSON);
    }

    function setTodayDeliveries(newDelivery) {
      service.orderString += newDelivery + ', ';
      var today = new Date().toLocaleDateString("en-US");
      var jsonString = '{' + '\"' + today + '\" : [' + trimComma() + ']}';
      localStorage.setItem('todayOrders', jsonString);
    }

    function trimComma() {
      return service.orderString.substring(0, service.orderString.length - 2);
    }

    function cleanTodayDeliveries() {
      localStorage.setItem('todayOrders', null);
    }

  }

})();
