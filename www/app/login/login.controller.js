(function () {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginCtrl', LoginCtrl);

  /* @ngInject */
  function LoginCtrl(ordersService,
                     $state,
                     $ionicPopup,
                     CORE) {

    var _this = this;
    _this.logIn = logIn;

    function logIn() {
      if ((_this.user.user == CORE.NORMAL_USER && _this.user.password == CORE.NORMAL_PASSWORD)
        || (_this.user.user == CORE.ADMIN_USER && _this.user.password == CORE.ADMIN_PASSWORD)) {
        ordersService.setUser(_this.user);
        $state.go('orders');
      } else {
        $ionicPopup.alert({
          title: 'Error de Autenticación',
          template: 'Autenticación Invalida',
          cssClass: 'text-center',
        });
      }
    }

    if (ordersService.getUser()) {
      $state.go('orders')
    }
  }
})();
